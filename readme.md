INSTALLATION GUIDE:

OS UBUNTU

REQUIREMENTS
-python3.5, 3.6 above
-mysql-server
-nginx/apache2 webserver
-django 2.0

INSTALL WEBSERVER OF YOUR CHOICE IF YOU WANT TO DEPLOY THIS PROJECT ON WEB SERVER
guide on nginx deploy: http://uwsgi-docs.readthedocs.io/en/latest/tutorials/Django_and_nginx.html

-for apache
apt-get install apache2
for nginx
apt-get install nginx

INSTALL MYSQL-SERVER
apt-get install mysql-server
apt-get install mysql-client

-after installing the database run the commands to avoid error on virtualenv
sudo apt-get install libpq-dev python3-dev libxml2-dev libxslt1-dev libldap2-dev libsasl2-dev libffi-dev
sudo apt-get install python3-dev 

INSTALL VIRTUALENV
apt-get install virtualenv

-after installing virtualenv activate your virtualenv
source /direnv/bin/activate
pip install django==2.0
pip install django-widget-tweaks
pip install django-settings
pip install mysql-python
pip install pymysql
pip install mysql-connector
pip install configparser
pip install mysqlclient

USAGE IN DAEMON APACHE2 WEBSERVER
-install
sudo apt install apache2-dev libapache2-mod-wsgi-py3
within the env wrapper
sudo pip3 install mod_wsgi

SAMPLE APACHE2 site configuration

#WSGIScriptAlias / /home/ubuntu/apps/gmovies-django-2.0/application/wsgi.py
WSGIPythonHome /home/ubuntu/apps/env
WSGIPythonPath /home/ubuntu/apps/gmovies-django-2.0/application

<VirtualHost *:80> 
	ServerName ec2-18-222-90-113.us-east-2.compute.amazonaws.com 

        <Directory /home/ubuntu/apps/gmovies-django-2.0/application>
        <Files wsgi.py>
        Require all granted
        </Files>
        </Directory>

        WSGIDaemonProcess ec2-18-222-90-113.us-east-2.compute.amazonaws.com python-home=/home/ubuntu/apps/env python-path=/home/ubuntu/apps/gmovies-django-2.0/application
        WSGIProcessGroup ec2-18-222-90-113.us-east-2.compute.amazonaws.com

        Alias /robots.txt /home/ubuntu/apps/gmovies-django-2.0/application/static/robots.txt
        Alias /favicon.ico /home/ubuntu/apps/gmovies-django-2.0/application/static/favicon.ico

        Alias /media/ /home/ubuntu/apps/gmovies-django-2.0/application/media/
        Alias /static/ /home/ubuntu/apps/gmovies-django-2.0/application/static/

        <Directory /home/ubuntu/apps/gmovies-django-2.0/application/static>
        Require all granted
        </Directory>

        <Directory /home/ubuntu/apps/gmovies-django-2.0/application/media>
        Require all granted
        </Directory>

        WSGIScriptAlias / /home/ubuntu/apps/gmovies-django-2.0/application/settings/wsgi.py

        <Directory /home/ubuntu/apps/gmovies-django-2.0/application/settings>
        <Files wsgi.py>
        Require all granted
        </Files>
        </Directory>
	
	ErrorLog /home/ubuntu/logs/gmovies-error.log
        CustomLog /home/ubuntu/logs/gmovies-access.log combined 
</VirtualHost> 
