from django.urls import path
from . import views

app_name = 'movie'
urlpatterns = [
    path('', views.Movie.as_view(), name='movie'),
    path('movie/title', views.MovieTitle.as_view(), name='movie-title'),
    path('movie/title/lookup', views.MovieTitleLookup.as_view(), name='movie-title-lookup'),
    #path('<int:pk>/', views.Detail.as_view(), name='detail'),
]