from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import *

class Movie(generic.ListView):
    template_name = 'movie/movie.html'
    context_object_name = 'latest_movie_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''

class MovieTitle(generic.ListView):
    template_name = 'movie/movie_title.html'
    context_object_name = 'latest_movie_title_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''

class MovieTitleLookup(generic.ListView):
    template_name = 'movie/movie_title_lookup.html'
    context_object_name = 'latest_movie_title_lookup_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''