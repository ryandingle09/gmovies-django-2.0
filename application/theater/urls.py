from django.urls import path
from . import views

app_name = 'theater'
urlpatterns = [
    path('', views.Theater.as_view(), name='theater'),
    path('organization', views.Organization.as_view(), name='organization'),
    #path('<int:pk>/', views.Detail.as_view(), name='detail'),
]