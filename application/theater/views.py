from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import *

class Theater(generic.ListView):
    template_name = 'theater/theater.html'
    context_object_name = 'latest_theater_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''

class Organization(generic.ListView):
    template_name = 'theater/organization.html'
    context_object_name = 'latest_organization_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''