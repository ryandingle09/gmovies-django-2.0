from django.urls import path
from . import views

app_name = 'setting'
urlpatterns = [
    path('promo-setting', views.Promo.as_view(), name='promo-setting'),
    path('system', views.System.as_view(), name='system'),
    path('convenience-fee', views.ConvenienceFee.as_view(), name='convenience-fee'),
    path('csr-campaign-setting', views.CsrCampaign.as_view(), name='csr-campaign-setting'),
    path('service', views.Service.as_view(), name='service'),
    #path('<int:pk>/', views.Detail.as_view(), name='detail'),
]