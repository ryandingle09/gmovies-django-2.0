from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import *

class Promo(generic.ListView):
    template_name = 'setting/promo.html'
    context_object_name = 'latest_promo_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''

class System(generic.ListView):
    template_name = 'setting/system.html'
    context_object_name = 'latest_system_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''

class ConvenienceFee(generic.ListView):
    template_name = 'setting/convenience-fee.html'
    context_object_name = 'latest_convenience_fee_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''

class CsrCampaign(generic.ListView):
    template_name = 'setting/csr-campaign.html'
    context_object_name = 'latest_csr_campaign_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''

class Service(generic.ListView):
    template_name = 'setting/service.html'
    context_object_name = 'latest_service_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''