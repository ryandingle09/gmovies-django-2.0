from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import *

class Transaction(generic.ListView):
    template_name = 'transaction/transaction.html'
    context_object_name = 'latest_transaction_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''

class FreeTicket(generic.ListView):
    template_name = 'transaction/free-ticket.html'
    context_object_name = 'latest_free_ticket_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''

class Promo(generic.ListView):
    template_name = 'transaction/promo.html'
    context_object_name = 'latest_promo_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''

class CsrCampaign(generic.ListView):
    template_name = 'transaction/csr-campaign.html'
    context_object_name = 'latest_csrc_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''