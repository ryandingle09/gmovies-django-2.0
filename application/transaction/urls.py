from django.urls import path
from . import views

app_name = 'transaction'
urlpatterns = [
    path('', views.Transaction.as_view(), name='transaction'),
    path('free-ticket', views.FreeTicket.as_view(), name='free-ticket'),
    path('promo', views.Promo.as_view(), name='promo'),
    path('csr-campaign', views.CsrCampaign.as_view(), name='csr-campaign'),
    #path('<int:pk>/', views.Detail.as_view(), name='detail'),
]