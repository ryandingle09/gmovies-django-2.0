from django.urls import path
from . import views

from django.conf.urls import url, include
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets

# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)

app_name = 'api'
urlpatterns = [
    path('v1', views.ApiV1.get, name='apiv1'),
    path('v2', views.ApiV2.as_view(), name='apiv2'),
    path('v3', views.ApiV3.as_view(), name='apiv3'),
    path('v4', views.ApiV4.as_view(), name='apiv4'),
    path('auth/', include('rest_framework.urls')),
    path('rest/', include(router.urls)),
]