from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import *

class Pesopay(generic.ListView):
    template_name = 'payment/pesopay.html'
    context_object_name = 'latest_pesopay_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''

class Paynamics(generic.ListView):
    template_name = 'payment/paynamics.html'
    context_object_name = 'latest_paynamics_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''

class Migs(generic.ListView):
    template_name = 'payment/migs.html'
    context_object_name = 'latest_migs_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''

class Ipay88(generic.ListView):
    template_name = 'payment/ipay88.html'
    context_object_name = 'latest_ipay88_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''

class Option(generic.ListView):
    template_name = 'payment/option.html'
    context_object_name = 'latest_option_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''