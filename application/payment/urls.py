from django.urls import path
from . import views

app_name = 'payment'
urlpatterns = [
    path('pesopay', views.Pesopay.as_view(), name='pesopay'),
    path('paynamics', views.Paynamics.as_view(), name='paynamics'),
    path('migs', views.Migs.as_view(), name='migs'),
    path('ipay88', views.Ipay88.as_view(), name='ipay88'),
    path('option', views.Option.as_view(), name='option'),
    #path('<int:pk>/', views.Detail.as_view(), name='detail'),
]