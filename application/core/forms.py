from django import forms
from django.contrib.auth import authenticate
from django.core.validators import validate_email
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class LoginForm(forms.Form):
    username = forms.CharField(max_length=100, required=True)
    password = forms.CharField(widget=forms.PasswordInput, max_length=100, required=True)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = authenticate(username=username, password=password)

            if not user or not user.is_active:
                raise forms.ValidationError("Sorry, Invalid username or password. Please try again.")

        return self.cleaned_data

class SignupForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

class PasswordResetForm(forms.Form):
    email = forms.EmailField(max_length=100, required=True)

    def clean(self):
        email = self.cleaned_data.get('email')

        if email and not User.objects.filter(email=email).exists():
            raise forms.ValidationError("We could'nt find that email address, Please try again.")

        return self.cleaned_data

class ChangePasswordForm(forms.Form):
    email = forms.EmailField(max_length=100, required=True, help_text='Required. Inform a valid and existing email address.')
    password = forms.CharField(widget=forms.PasswordInput, max_length=100, required=True)
    confirm_password = forms.CharField(widget=forms.PasswordInput, max_length=100, required=True)

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        confirm_password = self.cleaned_data.get('confirm_password')

        if not User.objects.filter(email=email).exists():
            raise forms.ValidationError("We could'nt find that email address, Please try again!")
        if password != confirm_password:
            raise forms.ValidationError("Password not match. Please try again!")

        return self.cleaned_data
