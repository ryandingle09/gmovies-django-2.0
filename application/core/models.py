import datetime
from django.db import models
from django.utils import timezone

class PasswordResets(models.Model):
	email = models.CharField(max_length=255)
	token = models.CharField(max_length=255)
	created_at 	= models.DateTimeField(timezone.now())
