from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.views import View
from django.utils import timezone
#from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.models import User
from .forms import *
from .models import *
from .core_helpers import *

#class handing for account login
class Login(View):
    template_name='core/login.html'

    def get(self, request, *args, **kwargs):
        form  = LoginForm()

        if request.user.is_authenticated:
            return redirect('/')

        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('/')

        form = LoginForm(request.POST or None)
        username = request.POST['username']
        password = request.POST['password']

        if form.is_valid():
            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)

                return HttpResponseRedirect('/')

        return render(request, self.template_name, {'form': form})

#class handing for account signup
class Signup(View):
    template_name = 'core/signup.html'

    def get(self, request, *args, **kwargs):
        form = SignupForm()
        if request.user.is_authenticated:
            return redirect('/')

        return render(request, self.template_name, {'form': form})
    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('/')

        form = SignupForm(request.POST or None)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            send_email(request, user, {}, 'Account activation email.', 'core/mail/account_activation.html')
            messages.success(request, 'Successfully registered, Please check activation link on your email.')

            return redirect('/accounts/success_signup/?email='+user.email)

        return render(request, self.template_name, {'form': form})

#class handling for successfull signup
class SuccessSignup(View):
    template_name = 'core/success_signup.html'
    
    def get(self, request, *args, **kwargs):
        email = request.GET['email']
        get_object_or_404(User, email=email)
        user =  User.objects.get(email=email)

        if user.is_active:
            raise Http404

        return render(request, self.template_name, {'email': email})

    def post(self, request, *args, **kwargs):
        email = request.POST['email']
        get_object_or_404(User, email=email)
        user =  User.objects.get(email=email)

        if user.is_active:
            raise Http404

        user = User.objects.get(email=email)
        send_email(request, user, {},'Account activation email.', 'core/mail/account_activation.html')
        messages.success(request, "Activation link successfully sent to your email "+email)

        return render(request, self.template_name, {'email': email})

#class handling for resetting password
class ResetPassword(View):
    template_name = 'core/password/reset_password.html'

    def get(self, request, *args, **kwargs):
        form = PasswordResetForm()

        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = PasswordResetForm(request.POST or None)
        email = request.POST['email']

        if form.is_valid():
            if User.objects.filter(email=email).exists():
                user = User.objects.get(email=email)
                token = generate_token.make_token(User.objects.get(email=email))
                email = email
                created_at = timezone.now()

                if PasswordResets.objects.filter(email=email).exists():
                    pr = PasswordResets.objects.get(email=email)
                    pr.email = email
                    pr.token = token
                    pr.created_at = created_at
                    pr.save()
                else:
                    pr = PasswordResets(email=email, token=token, created_at=created_at)
                    pr.save()

                messages.info(request, "Password reset link has been sent to your email.")
                data = {'token': token }
                send_email(request, user, {'token': token,},'Password reset link.', 'core/mail/password_reset.html')

        return render(request, self.template_name, {'form': form})

#class handling for resetting password
class ChangePassword(View):
    template_name = 'core/password/change_password.html'

    def get(self, request, token, *args, **kwargs):
        form = ChangePasswordForm()
        get_object_or_404(PasswordResets, token=token)

        return render(request, self.template_name, {'form': form})

    def post(self, request, token, *args, **kwargs):
        form = ChangePasswordForm(request.POST or None)
        email = request.POST['email']
        get_object_or_404(PasswordResets, token=token)

        if form.is_valid():
            user = User.objects.get(email=email)
            user.set_password(request.POST['password'])
            user.save()

            PasswordResets.objects.get(token=token).delete()
            messages.success(request, "You've successfully updated your password. You may now login with your new password!")

            return HttpResponseRedirect('/accounts/login/')

        return render(request, self.template_name, {'form': form})

#method for account activation via email link
def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64).decode())
        user = User.objects.get(pk=uid)

        if user.is_active:
            raise Http404
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and generate_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        messages.success(request, "You've Successfully activated your account.")

        return redirect('/')
    else:
        return render(request, 'account_activation_invalid.html')

#method for logout
@login_required
def logout_view(request):
    logout(request)

    return HttpResponseRedirect('/accounts/login')
