from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import *

class Index(generic.ListView):
    template_name = 'dashboard/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''