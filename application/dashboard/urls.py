from django.urls import path
from . import views

app_name = 'dashboard'
urlpatterns = [
    path('', views.Index.as_view(), name='dashboard'),
    #path('<int:pk>/', views.Detail.as_view(), name='detail'),
]