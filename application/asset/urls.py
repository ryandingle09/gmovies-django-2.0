from django.urls import path
from . import views

app_name = 'asset'
urlpatterns = [
    path('coming-soon', views.ComingSoon.as_view(), name='coming-soon'),
    path('blocked-screening', views.BlockedScreening.as_view(), name='blocked-screening'),
    #path('<int:pk>/', views.Detail.as_view(), name='detail'),
]