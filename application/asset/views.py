from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import *

class ComingSoon(generic.ListView):
    template_name = 'asset/coming-soon.html'
    context_object_name = 'latest_coming_soon_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''

class BlockedScreening(generic.ListView):
    template_name = 'asset/blocked-screening.html'
    context_object_name = 'latest_blocked_screening_list'

    def get_queryset(self):
        """
        return set of datas if needed
        """
        return ''