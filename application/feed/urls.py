from django.urls import path
from . import views

app_name = 'feed'
urlpatterns = [
    path('', views.Feed.as_view(), name='feed'),
    #path('<int:pk>/', views.Detail.as_view(), name='detail'),
]